package goutils

import (
	"sort"
	"testing"
)

//type StringMap map[string]interface{}
//type StringKeys []string
//func KeysFromMap(m StringMap, args ...bool) StringKeys {

func TestKeysFromMap(t *testing.T) {
	testMap := make(map[string]interface{})
	testMap["Key1"] = 4
	testMap["key1"] = "four"
	testMap["34"] = []byte("34")
	testMap["\\d"] = "next line"

	expected := []string{
		"Key1", "key1", "34", "\\d",
	}

	compare := KeysFromMap(testMap)
	sort.Strings(compare)
	sort.Strings(expected)

	if !compareSlices(compare, expected) {
		t.Errorf("Expected: %q, Got: %q", expected, compare)
	}
}

func compareSlices(s1, s2 []string) bool {
	if len(s1) != len(s2) {
		return false
	}

	for i, v := range s1 {
		if v != s2[i] {
			return false
		}
	}
	return true
}
