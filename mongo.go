package goutils

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Convenience function to keep existing API
func IsMongoCompressedCollection(m *mgo.Session, dbName string, collectionName string) {
	IsMongoCollection(m, dbName, collectionName, true)
}

// Requires WiredTiger storage engine
// If the collection exists, do nothing and return true.
// Otherwise, initialize the collection as compressed.
func IsMongoCollection(m *mgo.Session, dbName string, collectionName string, compressed bool) {
	names, _ := m.DB(dbName).CollectionNames()
	exists := false
	for _, v := range names {
		if v == collectionName {
			exists = true
		}
	}
	result := bson.M{}
	if !exists && compressed {
		m.DB(dbName).Run(bson.D{{"create", collectionName}, {"storageEngine", bson.M{"wiredTiger": bson.M{"configString": "block_compressor=snappy"}}}}, &result)
	} else if !exists {
		m.DB(dbName).C(collectionName)
	}
}
