package goutils

import (
	"fmt"
	"testing"
)

func TestSha1Hash(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{fmt.Sprint("68340", "Y224853514"), "KPm9FJDhgw1EwiKJKkyUTdMsUmE="},
	}

	for _, test := range tests {
		hash := Sha1Hash(test.input)
		if test.expected != hash {
			t.Errorf("Input=%q Expected=%q Got=%q", test.input, test.expected, hash)
		}
	}
}
