package goutils

import (
	"bytes"
	"io"
)

// add a byte converter to the io.reader type
// useful for processing http request body
func StreamToByte(stream io.Reader) []byte {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.Bytes()
}

//add a string converter to io.reader types
func StreamToString(stream io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.String()
}
