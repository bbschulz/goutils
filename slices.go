package goutils

import "strings"

func RemoveSliceElementString(s []string, i int) []string {
	newSlice := append(s[:i], s[i+1:]...)
	return newSlice
}

func RemoveSliceElementInt(s []int, i int) []int {
	newSlice := append(s[:i], s[i+1:]...)
	return newSlice
}

func RemoveSliceElementInt32(s []int32, i int) []int32 {
	newSlice := append(s[:i], s[i+1:]...)
	return newSlice
}

func RemoveSliceElementInt64(s []int64, i int) []int64 {
	newSlice := append(s[:i], s[i+1:]...)
	return newSlice
}

func RemoveSliceElementInterface(s []interface{}, i int) []interface{} {
	newSlice := append(s[:i], s[i+1:]...)
	return newSlice
}

// StringInSlice checks to see whether the str is a member of the slice of strings.
// The optional param ignoreCase compares after converting to upper case.
func StringInSlice(str string, list []string, ignoreCase ...bool) bool {
	_ignoreCase := false
	if len(ignoreCase) > 0 && ignoreCase[0] {
		_ignoreCase = true
		str = strings.ToUpper(str)
	}

	for _, v := range list {
		if _ignoreCase {
			v = strings.ToUpper(v)
		}

		if v == str {
			return true
		}
	}
	return false
}
