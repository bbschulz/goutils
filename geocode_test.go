package goutils

import (
	"math"
	"testing"
)

var geotests = []struct {
	lat1, long1 float64
	lat2, long2 float64
	expected    float64
}{
	{28.079991, -82.576800, 27.978809, -82.827984, 16.9},
}

func TestGeoDistance(t *testing.T) {
	for _, test := range geotests {
		dist := toFixed(GeoDistance(test.lat1, test.long1, test.lat2, test.long2), 1)
		if dist != test.expected {
			t.Errorf("Expected: %s, Got: %s \n", test.expected, dist)
		}
	}
}

// helper functions from http://stackoverflow.com/questions/18390266/how-can-we-truncate-float64-type-to-a-particular-precision-in-golang
func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}
