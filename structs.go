package goutils

import (
	"net/url"
	"reflect"
	"strconv"
)

// NOTE:  Functions to convert structs to maps is in package:
// github.com/lookingcloudy/structs

// StructToValues converts a struct into a url.values map
// used for sending a payload as query strings rather than a json body
// This does not currently handle embedded structs.  Also does not currently
// support tags.
func StructToValues(i interface{}) (values url.Values) {
	values = url.Values{}
	iVal := reflect.Indirect(reflect.ValueOf(i))
	typ := iVal.Type()
	for i := 0; i < iVal.NumField(); i++ {
		f := iVal.Field(i)
		// You can use tags here...
		// tag := typ.Field(i).Tag.Get("tagname")
		// Convert each type into a string for the url.Values string map
		var v string
		switch f.Interface().(type) {
		case int, int8, int16, int32, int64:
			v = strconv.FormatInt(f.Int(), 10)
		case uint, uint8, uint16, uint32, uint64:
			v = strconv.FormatUint(f.Uint(), 10)
		case float32:
			v = strconv.FormatFloat(f.Float(), 'f', 4, 32)
		case float64:
			v = strconv.FormatFloat(f.Float(), 'f', 4, 64)
		case []byte:
			v = string(f.Bytes())
		case string:
			v = f.String()
		}
		values.Set(typ.Field(i).Name, v)
	}
	return
}
