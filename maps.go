// utilitis to handle map values

package goutils

import "sort"

type StringMap map[string]interface{}
type StringKeys []string

// method for maps to return a slice of the keys from a map
// useful for subsequent sorting and iterating over a sorted map
func KeysFromMap(m StringMap, args ...bool) StringKeys {
	keys := make(StringKeys, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	if len(args) > 0 {
		if args[0] == true {
			sort.Strings(keys)
		}
	}
	return keys
}
