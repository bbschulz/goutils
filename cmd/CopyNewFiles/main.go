package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/bbschulz/goutils"
)

var sourceDir = flag.String("source", "", "Enter the full path of the source directory.  Enclose with quotes if spaces.")
var destDir = flag.String("dest", "", "Enter the full path of the destination directory.  Use quotes if spaces.")
var overwrite = flag.Bool("overwrite", false, "Specify whether to overwrite the destination if already exists.")
var recurse = flag.Bool("recurse", false, "Specify whether to recurse through the source folder.")
var force = flag.Bool("force", false, "Copy all source files to destination, whether or not they have already been copied")
var ignore = flag.String("ignore", "", "file containing list of files names to ignore")

func main() {

	flag.Parse()
	if *sourceDir == "" || *destDir == "" {
		flag.Usage()
		return
	}

	if _, err := os.Stat(*sourceDir); os.IsNotExist(err) {
		fmt.Printf("Source folder does not exist: %q \n", *sourceDir)
		flag.Usage()
		return
	}

	options := goutils.CopyOptions{
		Force:          *force,
		Overwrite:      *overwrite,
		NewOnly:        true,
		Print:          true,
		Recurse:        *recurse,
		IgnoreFileName: *ignore,
	}

	err := goutils.CopyDir(*sourceDir, *destDir, options)
	if err != nil {
		fmt.Println("Error copying folder: ", err)
	}
	return
}

func remove(someStrings []string, compare string) []string {
	for i, v := range someStrings {
		if strings.ToLower(v) == strings.ToLower(compare) {
			return append(someStrings[:i], someStrings[i+1:]...)
		}
	}
	return someStrings
}
