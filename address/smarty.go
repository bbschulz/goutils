package address

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

const (
	smartyMatch     string = "strict" // strict || range || invalid
	smartyBatchSize int    = 100
)

// SmartyService is an address service provider.  It implements the
// ProviderService interface and is used for accessing smarty Streets services
// Currently only the address validation service is implemented
type SmartyService struct {
	// inherit some things
	baseService

	authId    string
	authToken string
	batchSize int
	Client    http.Client
}

func ConfigSmarty(url string, auth string, token string) {
	s := SmartyService{
		authId:    auth,
		authToken: token,
		batchSize: smartyBatchSize,
		Client:    http.Client{},
	}
	s.setURI(url)
	Smarty = &s
	return
}

// Smarty is s default service provider with no authentication provided.
// Need to initialize with NewSmarty()
var Smarty *SmartyService = &SmartyService{}

func (self *SmartyService) BatchLimit() int { return self.batchSize }

func (self *SmartyService) Values() url.Values {
	values := url.Values{
		"auth-id":    {self.authId},
		"auth-token": {self.authToken},
	}
	return values
}

func (self *SmartyService) Lookup(r ProviderRequester) (ProviderResponser, error) {
	return r.Lookup()
}

func (self *SmartyService) LookupAddress(address Address) (ProviderResponser, error) {
	request := SmartyRequest{
		StreetNo:   address.StreetNo,
		Street:     address.Street,
		City:       address.City,
		State:      address.State,
		Zip:        address.Zip,
		Match:      "strict",
		Candidates: 2,
	}
	return request.Lookup()
}

func (self *SmartyService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	requests := SmartyRequests{}
	for _, address := range addresses {
		request := SmartyRequest{
			StreetNo:   address.StreetNo,
			Street:     address.Street,
			City:       address.City,
			State:      address.State,
			Zip:        address.Zip,
			Match:      "strict",
			Candidates: 2,
		}
		requests = append(requests, request)
	}
	return requests.Lookup()
}

//
// SmartyRequest is an ProviderRequester
// and implements that interface
// This is a single lookup request type
// no body and all parms passed as values
//
type SmartyRequest struct {
	InputId    string `json:"input_id,omitempty"`
	StreetNo   string `json:"-"`
	Street     string `json:"street"`
	City       string `json:"city"`
	State      string `json:"state"`
	Zip        string `json:"zipcode"`
	Match      string `json:"match,omitempty"`
	Candidates int    `json:"candidates,omitempty"`
}

func (self *SmartyRequest) Defaults() {
	if self.Match == "" {
		self.Match = "strict"
	}
	if self.Candidates == 0 {
		self.Candidates = 2
	}
}

func (self SmartyRequest) Values() url.Values {
	values := url.Values{
		"street":     {self.StreetNo + " " + self.Street},
		"city":       {self.City},
		"state":      {self.State},
		"zipcode":    {self.Zip},
		"match":      {smartyMatch},
		"candidates": {"2"},
	}
	return values
}
func (self SmartyRequest) Body() []byte { return []byte{} }

func (self SmartyRequest) Lookup() (ProviderResponser, error) {
	response := SmartyResponses{}
	err := get(self.Service(), self, &response)
	return response, err
}

func (self SmartyRequest) Service() ProviderService { return Smarty }

// SmartyRequests is a ProviderRequester.  This is a post request
// with all values passed in the body.
type SmartyRequests []SmartyRequest

func (self SmartyRequests) Values() url.Values { return url.Values{} }

func (self SmartyRequests) Body() []byte {
	// first need to add the streetNO to the street for all the requests
	newRequests := SmartyRequests{}
	for _, r := range self {
		r.Street = r.StreetNo + " " + r.Street
		newRequests = append(newRequests, r)
	}

	body, err := json.Marshal(newRequests)
	if err != nil {
		fmt.Println("Error Marshalling Request ", err)
	}
	return body
}

func (self SmartyRequests) Lookup() (ProviderResponser, error) {
	// make sure not over 100
	if len(self) > Smarty.BatchLimit() {
		return nil, errors.New("More than 100 request records")
	}
	response := SmartyResponses{}
	err := post(self.Service(), self, &response)
	return response, err
}

func (self SmartyRequests) Service() ProviderService { return Smarty }

type SmartyMetadata struct {
	RecordType   string  `json:"record_type"`
	ZipType      string  `json:"zip_type"`
	CountyFips   string  `json:"county_fips"`
	CountyName   string  `json:"county_name"`
	CarrierRoute string  `json:"carrier_route"`
	RDI          string  `json:"rdi"`
	ElotSequence string  `json:"elot_sequence"`
	ElotSort     string  `json:"elot_sort"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Precision    string  `json:"precision"`
	TimeZone     string  `json:"time_zone"`
	UtcOffset    int64   `json:"utc_offset"`
	DST          bool    `json:"dst"`
}

type SmartyAnalysis struct {
	DpvMatchCode string `json:"dpv_match_code"`
	DpvFootnotes string `json:"dpv_footnotes"`
	DpvCmra      string `json:"dpv_cmra"`
	DpvVacant    string `json:"dpv_vacant"`
	Active       string `json:"active"`
	Footnotes    string `json:"footnotes"`
}

type SmartyResponse struct {
	InputIndex     int            `json:"input_index"`
	CandidateIndex int            `json:"candidate_index"`
	DeliveryLine1  string         `json:"delivery_line_1"`
	LastLine       string         `json:"last_line"`
	Metadata       SmartyMetadata `json:"metadata"`
	Analysis       SmartyAnalysis `json:"analysis"`
}

func (self SmartyResponse) Response(index int) ProviderResponser { return self }

type SmartyResponses []SmartyResponse

func (self SmartyResponses) Response(index int) ProviderResponser {

	responses := SmartyResponses{}
	if index < 0 || index > len(self) {
		return responses
	}

	for _, v := range self {
		if v.InputIndex == index {
			responses = append(responses, v)
		}
	}
	return responses
}
