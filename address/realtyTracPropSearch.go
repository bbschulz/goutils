package address

// Property Search Service

var RealtyTrackPropSearch *RealtyTrackPropSearchService = &RealtyTrackPropSearchService{}

type RealtyTrackPropSearchService struct {
	RealtyTrackService
}

func ConfigRTPropSearchService(url string, apikey string) {
	service := RealtyTrackPropSearchService{
		RealtyTrackService{
			APIKey: apikey,
		},
	}
	service.setURI(url)
	RealtyTrackPropSearch = &service
}

func (self *RealtyTrackPropSearchService) LookupAddress(address Address) (ProviderResponser, error) {
	// realtyTrac searches the street components separately
	prefix, streetName, suffix := splitStreet(address.Street)
	request := RealtyTracPropSearchRequest{
		RealtyTracRequest{
			AddressNumber: address.StreetNo,
			StreetName:    streetName,
			StreetDir:     prefix,
			StreetSuffix:  suffix,
			City:          address.City,
			StateCode:     address.State,
			ZipCode:       address.Zip,
		},
	}
	request.Defaults()
	return request.Lookup()
}

func (self *RealtyTrackPropSearchService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	// realtyTrac does not implement a batch lookup API...this executes each one separately
	// and assembles the results
	results := RealtyTracResponses{}
	for i, a := range addresses {
		result, _ := self.LookupAddress(a)
		results[i] = result.(RealtyTracResponse)
	}
	return results, nil
}

// Inherits from basic request...including the methods
type RealtyTracPropSearchRequest struct {
	RealtyTracRequest
}

// note: defaults() is not part of the ProverRequester interface, so this method
// can use a pointer receiver while the other methods do not
func (self *RealtyTracPropSearchRequest) Defaults() {
	self.AddressType = "Both"
	self.NumberOfRecords = 2
	self.Format = "JSON"
	self.ReportID = 105
	self.Sort = "ASC"
}

func (self *RealtyTracPropSearchRequest) Lookup() (ProviderResponser, error) {
	response := RealtyTracResponse{}
	err := get(self.Service(), self, &response)
	return response, err
}
func (self *RealtyTracPropSearchRequest) Service() ProviderService {
	return RealtyTrackPropSearch
}
