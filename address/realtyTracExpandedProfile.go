package address

// EXPANDED PROFILE SERVICE
var RealtyTrackExpandedProfile *RealtyTrackExpandedProfileService

// Inherits from base class, including all the methods
type RealtyTrackExpandedProfileService struct {
	RealtyTrackService
}

func ConfigRTExpandedProfileService(url string, apikey string) {
	service := RealtyTrackExpandedProfileService{
		RealtyTrackService{
			APIKey: apikey,
		},
	}
	service.setURI(url)
	RealtyTrackExpandedProfile = &service
}

func (self *RealtyTrackExpandedProfileService) LookupAddress(address Address) (ProviderResponser, error) {
	// realtyTrac searches the street components separately
	request := RealtyTracExpandedProfileRequest{
		RealtyTracRequest{
			PropertyStreetAddress: address.StreetNo + " " + address.Street,
			City:      address.City,
			StateCode: address.State,
			ZipCode:   address.Zip,
		},
	}
	request.Defaults()
	return request.Lookup()
}

func (self *RealtyTrackExpandedProfileService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	// realtyTrac does not implement a batch lookup API...this executes each one separately
	// and assembles the results
	results := RealtyTracResponses{}
	for i, a := range addresses {
		result, _ := self.LookupAddress(a)
		results[i] = result.(RealtyTracResponse)
	}
	return results, nil
}

type RealtyTracExpandedProfileRequest struct {
	RealtyTracRequest
}

// note: defaults() is not part of the ProverRequester interface, so this method
// can use a pointer receiver while the other methods do not
func (self *RealtyTracExpandedProfileRequest) Defaults() {
	self.AddressType = "Both"
	self.NumberOfRecords = 2
	self.Format = "JSON"
	self.ReportID = 102
	self.Sort = "ASC"
}

func (self *RealtyTracExpandedProfileRequest) Service() ProviderService {
	return RealtyTrackExpandedProfile
}

func (self *RealtyTracExpandedProfileRequest) Lookup() (ProviderResponser, error) {
	response := RealtyTracResponse{}
	err := get(self.Service(), self, &response)
	return response, err
}
