// The address package is a collection of address related 3rd party webservices that lookup information
// based on an address or geocode.  Each of the adapters has its own Request and Response types that are
// native to the 3rd party web service.  In addition, many of the packages support looking up information
// based oa a standardized address.  This standardized address is provided as address.Address.
package address

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

/*
	The address package is a set of address based services and utilities.  It provides a standard
	interface for using 3rd party address services such as Smarty Streets.

	The idea is that each service endpoint has it's own service type.  For example, RealtyTrac has serveral different
	API's - each with a different endpoint.  Each API Endpoint has a unique ProviderService type.

	Each service implements custom ProviderRequester(s) and ProviderResponsor(s) specific to that interface.  This
	way full functionality of the service can be utilized.  However, the key concept of this module is that
	each ProviderService also understands how to use a common Address object.   Within a client application,
	 Address objects can be loaded up and used to execute against multiple ProviderService(s) without the client
	 needing to understand the nuances of each different Request/Response.
*/

// A ProviderService is a service that can make calls against a 3rd party
// service.  A service must also implement one or more ProviderRequesters and
// one or more ProviderResponser types.
type ProviderService interface {
	// Lookup provides a basic lookup against request using
	// the naitive request type
	Lookup(ProviderRequester) (ProviderResponser, error)

	// A special case of looking up based on an Address object
	LookupAddress(Address) (ProviderResponser, error)

	// Special case of looking up based on a batch of addresses
	LookupAddresses([]Address) (ProviderResponser, error)

	// returns the URI string for the service
	URI() string

	// in the case of batching multiple requests, this returns the limit
	// based on the service.  The methods do not split requests if they are
	// over the limit.  Chunking requests is the responsibility of the client.
	BatchLimit() int

	// Values returns the query param values required by the service.  Typically,
	// this returns authentication values.  These are in addition to any
	// parameters implemented as part of the ProviderRequester interfaces.
	Values() url.Values

	// gets the error of the last operation
	Error() error

	// set the error of the last operation
	setError(error)

	// set the URI
	setURI(string)
}

// A ProviderRequester is the implementation of any type
// used to initiate a lookup against a service.
// Each ServiceProvider implements one or more ProviderRequesters.
type ProviderRequester interface {
	// Lookup() is the central method of the interface
	// against the native request objects.
	Lookup() (ProviderResponser, error)

	// Values returns the header values required
	// by the implementation.  Typically, data is passed
	// in the query string or in the body.
	Values() url.Values

	// Body returns any data that will be sent in
	// the body of the message.  Usually formatted
	// as JSON.
	Body() []byte

	// returns the service provider for the request object
	Service() ProviderService
}

// The Response() method is only valid for indexing multiple return items.
// for example, if a batch of requests is sent, the response for a particular
// request is returned.  For a single result, that result is returned
// regardless of the index provided
type ProviderResponser interface {
	Response(int) ProviderResponser
}

// Address is a generic service type - each provider needs to understand
// how to lookup based on this type
type Address struct {
	StreetNo string
	Street   string
	City     string
	State    string
	Zip      string
}

// Error object to be used for all service providers
type Error struct {
	Status string
	Body   string
	Err    error
}

func (err Error) Error() string {
	return fmt.Sprintf("Status: %s / Body: %s / Error: %s \n", err.Status, err.Body, err.Err)
}

// this is a base class to save a little work that is common to all providers
// note that service receivers are implemented as pointers
type baseService struct {
	uri string
	err error
}

func (self *baseService) URI() string {
	return self.uri
}

func (self *baseService) Error() error {
	return self.err
}

func (self *baseService) setError(err error) {
	self.err = err
}

func (self *baseService) setURI(url string) {
	self.uri = url
}

// helper functions
// request is a generic request function that passes header values and a body.  The any valid method is supported.
func request(svc ProviderService, method string, values url.Values, body []byte, w ProviderResponser) error {
	var err error
	// the provider may have some
	// header values that need to
	// be included also
	for k, v := range svc.Values() {
		values[k] = v
	}

	request, _ := http.NewRequest(method, svc.URI()+"?"+values.Encode(), bytes.NewReader(body))
	client := http.Client{Timeout: time.Second * 20}
	// DEBUG
	//fmt.Println("Request URL: ", request.URL)
	//fmt.Println("Request URI(): ", svc.URI())
	//fmt.Println(string(body))
	//
	//buf := new(bytes.Buffer)
	//buf.ReadFrom(request.Body)
	//newStr := buf.String()
	//fmt.Println(newStr)

	// clear the error object
	svc.setError(nil)

	resp, err := client.Do(request)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil || !strings.Contains(resp.Status, "200") {
		buf := new(bytes.Buffer)
		var status string
		if resp != nil {
			buf.ReadFrom(resp.Body)
			status = resp.Status
		}
		newErr := Error{
			Status: status,
			Body:   buf.String(),
			Err:    err,
		}
		svc.setError(newErr)
		return newErr
	}

	// capture the body as a string in case it cannot be decoded into w
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		log.Println("Error")
		newErr := Error{
			Status: resp.Status,
			Err:    err,
		}
		svc.setError(newErr)
		return newErr
	}

	err = json.Unmarshal(buf.Bytes(), w)
	if err != nil {
		log.Println("error")
		newErr := Error{
			Status: resp.Status,
			Body:   buf.String(),
			Err:    err,
		}
		svc.setError(newErr)
		return newErr
	}

	//fmt.Println(buf.String())

	return nil
}

func get(svc ProviderService, r ProviderRequester, w ProviderResponser) error {
	return request(svc, "GET", r.Values(), r.Body(), w)
}

func post(svc ProviderService, r ProviderRequester, w ProviderResponser) error {
	return request(svc, "POST", r.Values(), r.Body(), w)
}

// unique list of valid street suffixes.  run the following in mongo to refresh the list:
// 	mongodb > mongo> db.zip4.aggregate([ {$group: { _id: "$street_suffix"} }])
var streetSuffixes = map[string]bool{
	"MNRS": true, "FLTS": true, "LCK": true, "CLFS": true, "SPG": true, "CORS": true, "DV": true, "CTS": true, "BRG": true,
	"TRWY": true, "SHR": true, "FRY": true, "ARC": true, "MTN": true, "RIV": true, "MDW": true, "ML": true, "FRK": true,
	"EST": true, "CLF": true, "SMT": true, "BLFS": true, "TPKE": true, "XRD": true, "BTM": true, "KNLS": true, "EXPY": true,
	"RADL": true, "CRSE": true, "RST": true, "FWY": true, "GRV": true, "CURV": true, "UN": true, "MTWY": true, "SHL": true,
	"CMNS": true, "GDN": true, "CRK": true, "SQ": true, "RTE": true, "HVN": true, "MALL": true, "FLS": true, "FLT": true,
	"CSWY": true, "BRK": true, "HBR": true, "BCH": true, "EXT": true, "PARK": true, "COR": true, "GDNS": true, "MLS": true,
	"PLZ": true, "VW": true, "CT": true, "BR": true, "RUE": true, "WAY": true, "KY": true, "ST": true, "GTWY": true, "VIA": true,
	"CRST": true, "HOLW": true, "CV": true, "PSGE": true, "AVE": true, "BLVD": true, "DR": true, "ROW": true, "XRDS": true,
	"RUN": true, "CLB": true, "KNL": true, "HWY": true, "LAND": true, "PATH": true, "VLY": true, "PL": true, "PKWY": true,
	"IS": true, "CYN": true, "ISLE": true, "RD": true, "TER": true, "PT": true, "MEWS": true, "FRST": true, "TRL": true,
	"LDG": true, "CTR": true, "LN": true, "STA": true, "CIR": true, "WALK": true, "BYP": true, "MNR": true, "CMN": true, "LNDG": true,
	"INLT": true, "SPGS": true, "LK": true, "GRVS": true, "ALY": true, "SHRS": true, "XING": true, "HL": true, "BLF": true,
	"MDWS": true, "PRT": true, "GRN": true, "RDG": true, "PASS": true, "WLS": true, "PNES": true, "CRES": true, "STRM": true,
	"LKS": true, "GLN": true, "LOOP": true, "PNE": true, "FRD": true, "ANX": true, "OVAL": true, "ESTS": true, "PTS": true,
	"TRAK": true, "BYU": true, "BND": true, "SPUR": true, "VLG": true, "DL": true, "JCT": true, "PIKE": true, "FLD": true,
	"VIS": true, "HTS": true, "CP": true, "TRCE": true, "FRG": true, "RNCH": true, "HLS": true,
}

// unique list of street prefix directions.  run the following in mongo to refresh the list
// 	mongodb > db.zip4.aggregate([ {$group: { _id: "$street_pre_dir"} }])
var streetDirections = map[string]bool{
	"SE": true,
	"S":  true,
	"NW": true,
	"N":  true,
	"NE": true,
	"E":  true,
	"W":  true,
	"SW": true,
}

// splitStreet() splits apart a single street string into its 3 parts
// this does not separate house numbers
func splitStreet(street string) (streetPrefix string, streetName string, streetSuffix string) {
	split := strings.Split(street, " ")
	for _, v := range split {
		v = strings.ToUpper(v)
		if streetDirections[v] {
			streetPrefix = v
		} else if streetSuffixes[v] {
			streetSuffix = v
		} else {
			streetName = strings.TrimSpace(streetName + " " + v)
		}
	}
	return
}
