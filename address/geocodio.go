package address

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

// GeoCodeService is an address service provider.  It implements the
// ProviderService interface and is used for accessing Geocod.io Geocoding services
// Currently only the address validation service is implemented
type GeoCodeService struct {
	// inherit some basic things
	baseService

	apiKey    string
	batchSize int
	Client    http.Client
}

func ConfigGeocodio(url string, apiKey string, batch int) {
	g := GeoCodeService{
		apiKey:    apiKey,
		batchSize: batch,
	}
	g.setURI(url)
	Geocodio = &g
	return
}

// need to initialize GeoCodio before it can be used.
// Call ConfigGeocodio()
var Geocodio *GeoCodeService

func (self *GeoCodeService) BatchLimit() int { return self.batchSize }

func (self *GeoCodeService) Values() url.Values {
	values := url.Values{
		"api_key": {Geocodio.apiKey},
	}
	return values
}

func (self *GeoCodeService) Lookup(r ProviderRequester) (ProviderResponser, error) {
	return r.Lookup()
}

func (self *GeoCodeService) LookupAddress(address Address) (ProviderResponser, error) {
	request := GeocodRequest{
		Street: address.StreetNo + " " + address.Street,
		City:   address.City,
		State:  address.State,
		Zip:    address.Zip,
	}
	return request.Lookup()
}

// todo: geocode supports batch, but getting back a 422 Unprocessable Entity
//func (self *GeoCodeService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
//	requests := GeocodRequests{}
//	for _, address := range addresses {
//		request := GeocodRequest{
//			Street: address.StreetNo + " " + address.Street,
//			City:   address.City,
//			State:  address.State,
//			Zip:    address.Zip,
//		}
//		requests = append(requests, request)
//	}
//	return requests.Lookup()
//}

func (self *GeoCodeService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	results := GeocodResponses{}
	for _, a := range addresses {
		result, _ := self.LookupAddress(a)
		results = append(results, result.(GeocodResponse))
	}
	return results, nil
}

func (self *GeoCodeService) Key() string {
	return self.apiKey
}

// *********************************************
// ************** REQUEST **********************

// There is only a single request interface for this service
// This implements the ProviderRequester interface
type GeocodRequest struct {
	Street string `json:"street"`
	City   string `json:"city"`
	State  string `json:"state"`
	Zip    string `json:"postal_code"`
}

func (self GeocodRequest) Values() url.Values {
	values := url.Values{
		"street":      {self.Street},
		"city":        {self.City},
		"state":       {self.State},
		"postal_code": {self.Zip},
	}
	return values
}
func (self GeocodRequest) Body() []byte { return []byte{} }

func (self GeocodRequest) Lookup() (ProviderResponser, error) {
	response := GeocodResponse{}
	err := get(self.Service(), self, &response)
	return response, err
}

func (self GeocodRequest) Service() ProviderService { return Geocodio }

// GeocodRequests is a ProviderRequester.  This is a post request
// with all values passed in the body.
type GeocodRequests []GeocodRequest

func (self GeocodRequests) Values() url.Values { return url.Values{} }

func (self GeocodRequests) Body() []byte {

	body, err := json.Marshal(self)
	if err != nil {
		fmt.Println("Error Marshalling Request ", err)
	}
	return body
}

func (self GeocodRequests) Lookup() (ProviderResponser, error) {
	if len(self) > self.Service().BatchLimit() {
		return nil, errors.New("More than maximum request records")
	}
	response := GeocodResponse{}
	err := post(self.Service(), self, &response)
	return response, err
}

func (self GeocodRequests) Service() ProviderService { return Geocodio }

type GeocodLocation struct {
	Lat  float64 `json:"lat"`
	Long float64 `json:"lng"`
}

// results are an array of this object
// See https://geocod.io/docs/#accuracy-score
type GeocodResults struct {
	// address_components - we are omitting this...don't care about this data
	// formatted_address - omitting

	// contains geo coordinates
	Location GeocodLocation `json:"location"`

	// decimal value from 0.0 - 1.0.  A value of 1 is rooftop.  Over 0.8 should be pretty good.
	Accuracy float64 `json:"accuracy"`

	// rooftop = exact point
	// point = found exact single point through interpolation...should be accurate
	// range_interpolation = probably not accurate
	// street_center
	// place
	// state
	AccuracyType string `json:"accuracy_type"`

	Source string `json:"source"`
}

type GeocodResponse struct {
	Results []GeocodResults `json:"results"`
}

func (self GeocodResponse) Response(index int) ProviderResponser { return self }

type GeocodResponses []GeocodResponse

func (self GeocodResponses) Response(index int) ProviderResponser {
	responses := GeocodResponses{}
	if index < 0 || index > len(self) {
		return responses
	}

	response := self[index]

	// if empty response
	if len(response.Results) == 0 {
		return nil
	}

	return response
}
