package address

import (
	"log"
	"net/http"
	"net/url"

	"encoding/json"

	"strconv"

	"github.com/pkg/errors"
)

// Intermap documentation:  https://analytics.api.intermap.com/api/swagger/ui/index
// for the tangiblyUnique interface
type intermap_tangUnique_service struct {
	// inherit some basic things
	baseService

	apiKey    string
	batchSize int
	Client    http.Client
}

func ConfigIntermap(url string, apiKey string, batch int) {
	g := intermap_tangUnique_service{
		apiKey:    apiKey,
		batchSize: batch,
	}
	g.setURI(url)
	Intermap = &g
	return
}

var Intermap *intermap_tangUnique_service

func (self *intermap_tangUnique_service) BatchLimit() int { return self.batchSize }

func (self *intermap_tangUnique_service) Values() url.Values {
	values := url.Values{
		"api_key": {self.apiKey},
	}
	return values
}

func (self *intermap_tangUnique_service) Lookup(r ProviderRequester) (ProviderResponser, error) {
	return r.Lookup()
}

func (self *intermap_tangUnique_service) LookupAddress(address Address) (ProviderResponser, error) {
	return nil, errors.New("Intermap does not support lookup based on address")
}

func (self *intermap_tangUnique_service) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	return nil, errors.New("Intermap does not support lookup based on address")
}

func (self *intermap_tangUnique_service) Key() string {
	return self.apiKey
}

// intermap base types
type intermapSchema struct {
	// Type = "Feature"
	Type       string              `json:"type"`
	Geometry   intermapGeometry    `json:"geometry"`
	Properties *intermapProperties `json:"properties,omitempty"`
}

type intermapGeometry struct {
	// Type = "Point"
	Type string `json:"type"`

	// this is populated automatically from the provided lat/long values
	Coordinates []float64 `json:"coordinates"`

	Lat  float64 `json:"-"`
	Long float64 `json:"-"`
}

type intermapProperties struct {
	// elevation is in meters
	DerivedBaseFloodElevation *float64 `json:"derivedBfe"`
	Elevation                 *float64 `json:"elevation"`
	FemaBaseFloodElevation    *float64 `json:"femaBfe"`
	FemaZone                  *string  `json:"femaZone"`

	//  The quality of the geocoding result, if input coordinates were entered in form of address; null otherwise
	GeoCodeQuality *string `json:"geocodingQuality"`

	HeightOfMinSurge *float64 `json:"heightOfMinSurge"`

	IsInCoverage *bool   `json:"isInCoverage"`
	Lat          float64 `json:"latitude"`
	Long         float64 `json:"longitude"`

	// The weakest category storm that has inundation at this location. "Cat 0" to "Cat 5
	// "Coastal" = Cat 0, Cat 1, Cat 2, Cat 3
	MinimumStormCat *string `json:"minStormCat"`
	MinimumSurgeCat *string `json:"minSurgeCat"`

	// OceanDistance in meters, only if < 30km,
	OceanDistance *float64 `json:"oceanDistance"`

	// Riskscore is 0-100, lower is better
	RiskScore *float64 `json:"riskScore"`

	// Storm frequency is the indicator of the frequency of hurricane events occurring at the input coordinate.
	// This Frequency information is based on HURDAT2 data. Possible values are
	// 1 = Low, 2 = Moderate, 3 = High, 4 = Very High, 5 = Extremely High
	SurgeFrequency *int `json:"surgeFrequency"`
	StormFrequency *int `json:"stormFrequency"`

	// Surge high tide water height in meters above MSL
	SurgeCat0Height *float64 `json:"surgeCat0Height"`
	SurgeCat1Height *float64 `json:"surgeCat1Height"`
	SurgeCat2Height *float64 `json:"surgeCat2Height"`
	SurgeCat3Height *float64 `json:"surgeCat3Height"`
	SurgeCat4Height *float64 `json:"surgeCat4Height"`
	SurgeCat5Height *float64 `json:"surgeCat5Height"`
}

type IntermapRequest struct {
	intermapSchema
}

func (self IntermapRequest) New(lat, long float64) IntermapRequest {
	obj := IntermapRequest{}
	obj.Type = "Feature"
	obj.Geometry.Lat = lat
	obj.Geometry.Long = long
	obj.Geometry.Coordinates = []float64{long, lat}
	//obj.Geometry.Coordinates = []float64{lat, long}
	return obj
}

func (self IntermapRequest) Values() url.Values {
	values := url.Values{
		"easting":  {strconv.FormatFloat(self.Geometry.Long, 'f', -1, 64)},
		"northing": {strconv.FormatFloat(self.Geometry.Lat, 'f', -1, 64)},
		"crs":      {"EPSG:4326"},
	}
	return values
}
func (self IntermapRequest) Body() []byte { return []byte{} }

func (self IntermapRequest) Lookup() (ProviderResponser, error) {
	response := IntermapResponse{}
	err := get(self.Service(), self, &response)
	return response, err
}

func (self IntermapRequest) Service() ProviderService { return Intermap }

func (self IntermapRequest) ConvertToResponse() IntermapResponse {
	resp := IntermapResponse{}
	resp.intermapSchema = self.intermapSchema
	return resp
}

type IntermapResponse struct {
	intermapSchema
}

func (self IntermapResponse) Response(index int) ProviderResponser { return self }

func (self IntermapResponse) ConvertToRequest() IntermapRequest {
	req := IntermapRequest{}
	req.intermapSchema = self.intermapSchema
	return req
}

type intermapCollectionSchema struct {
	// "FeatureCollection"
	Type     string            `json:"type"`
	Features []IntermapRequest `json:"features"`
}

// don't really need to inherit like this, but doing for potential future reuse
type IntermapRequests struct {
	intermapCollectionSchema
}

func (self IntermapRequests) New() IntermapRequests {
	r := IntermapRequests{}
	r.Type = "FeatureCollection"
	r.Features = []IntermapRequest{}
	return r
}

func (self IntermapRequests) Service() ProviderService { return Intermap }

func (self IntermapRequests) Values() url.Values { return url.Values{} }

func (self IntermapRequests) Body() []byte {
	body, err := json.Marshal(self)
	if err != nil {
		log.Println("Intermap error: ", err)
	}
	return body
}

func (self IntermapRequests) Lookup() (ProviderResponser, error) {
	responses := IntermapResponses{}
	err := post(self.Service(), self, &responses)
	return responses, err
}

type IntermapResponses struct {
	intermapCollectionSchema
}

func (self IntermapResponses) Response(index int) ProviderResponser {
	if index < 0 || index > len(self.Features) {
		return nil
	}

	// have to convert an IntermapRequest to an IntermapResponse
	item := self.Features[index]
	resp := IntermapResponse{}
	resp.intermapSchema = item.intermapSchema

	return resp
}
