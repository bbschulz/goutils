package address

import (
	"net/url"

	"bitbucket.org/bbschulz/goutils"
)

// PROPERTY SEARCH

// Note: this basic structure and most methods are the same for each service.
// only the LookupAddress() methods are different for each service due to the different
// statically typed requests.
//
// Therefore, this object does not fully implement the ProviderService interface.
// The sub-types must implement the LookupAddress() methods to fully fulfill the interface
// definition.
type RealtyTrackService struct {
	baseService
	APIKey string
}

func (self *RealtyTrackService) Lookup(r ProviderRequester) (ProviderResponser, error) {
	return r.Lookup()
}

func (self *RealtyTrackService) BatchLimit() int { return 1 }

func (self *RealtyTrackService) Values() url.Values {
	return url.Values{
		"ApiKey": {self.APIKey},
	}
}

// RealtyTracRequest is the same for all the services
// Do not use this type directly
type RealtyTracRequest struct {
	PropertyStreetAddress string
	AddressType           string // "Both"
	AddressNumber         string
	StreetDir             string
	StreetName            string
	StreetSuffix          string
	City                  string
	StateCode             string
	ZipCode               string
	PropertyParcelID      string
	APN                   string
	ApnRangeStart         string
	ApnRangeEnd           string
	Latitude              string
	Longitude             string
	Radius                int
	SearchType            string
	NumberOfRecords       int    // 10
	Sort                  string // ASC
	Format                string // JSON
	ReportID              int    // different for each report
}

func (self RealtyTracRequest) Values() url.Values { return goutils.StructToValues(self) }

func (self RealtyTracRequest) Body() []byte { return []byte{} }

type RealtyTracResponse map[string]interface{}

func (self RealtyTracResponse) Response(index int) ProviderResponser {
	return self
}

// handles batch requests.  The map index is associated to the index of the search request
// []address -> batch of requests.  response[1] is the response for address[1]
type RealtyTracResponses map[int]RealtyTracResponse

func (self RealtyTracResponses) Response(index int) ProviderResponser {
	response, ok := self[index]
	if ok {
		return response
	}
	return RealtyTracResponse{}
}
