package address

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

/* SmartyZip implements the zipcode service for Smarty Streets

 */

type SmartyZipService struct {
	// inherit some things
	baseService

	authId    string
	authToken string
	batchSize int
	Client    http.Client
}

func ConfigSmaryZipService(url string, auth string, token string) {
	s := SmartyZipService{
		authId:    auth,
		authToken: token,
		batchSize: 100,
		Client:    http.Client{},
	}
	s.setURI(url)
	SmartyZip = &s
	return
}

var SmartyZip *SmartyZipService

func (self *SmartyZipService) BatchLimit() int { return self.batchSize }

func (self *SmartyZipService) Values() url.Values {
	values := url.Values{
		"auth-id":    {self.authId},
		"auth-token": {self.authToken},
	}
	return values
}

func (self *SmartyZipService) Lookup(r ProviderRequester) (ProviderResponser, error) {
	return r.Lookup()
}

func (self *SmartyZipService) LookupAddress(address Address) (ProviderResponser, error) {
	request := SmartyRequest{
		StreetNo:   address.StreetNo,
		Street:     address.Street,
		City:       address.City,
		State:      address.State,
		Zip:        address.Zip,
		Match:      "strict",
		Candidates: 2,
	}
	return request.Lookup()
}

func (self *SmartyZipService) LookupAddresses(addresses []Address) (ProviderResponser, error) {
	requests := SmartyRequests{}
	for _, address := range addresses {
		request := SmartyRequest{
			StreetNo:   address.StreetNo,
			Street:     address.Street,
			City:       address.City,
			State:      address.State,
			Zip:        address.Zip,
			Match:      "strict",
			Candidates: 2,
		}
		requests = append(requests, request)
	}
	return requests.Lookup()
}

type SmartyZipRequest struct {
	City  string `json:"city"`
	State string `json:"state"`
	Zip   string `json:"zipcode"`
}

func (self SmartyZipRequest) Values() url.Values {
	values := url.Values{
		"city":    {self.City},
		"state":   {self.State},
		"zipcode": {self.Zip},
	}
	return values
}
func (self SmartyZipRequest) Body() []byte { return []byte{} }

func (self SmartyZipRequest) Lookup() (ProviderResponser, error) {
	response := SmartyZipResponses{}
	err := get(self.Service(), self, &response)
	return response, err
}

func (self SmartyZipRequest) Service() ProviderService { return SmartyZip }

type SmartyZipRequests []SmartyZipRequest

func (self SmartyZipRequests) Values() url.Values { return url.Values{} }

func (self SmartyZipRequests) Body() []byte {
	// first need to add the streetNO to the street for all the requests
	body, err := json.Marshal(self)
	if err != nil {
		fmt.Println("Error Marshalling Request ", err)
	}
	return body
}

func (self SmartyZipRequests) Lookup() (ProviderResponser, error) {
	// make sure not over 100
	if len(self) > Smarty.BatchLimit() {
		return nil, errors.New("More than 100 request records")
	}
	response := SmartyZipResponses{}
	err := post(self.Service(), self, &response)
	return response, err
}

func (self SmartyZipRequests) Service() ProviderService { return SmartyZip }

type SmartyZipState struct {
	City         string `json:"city"`
	StateAbbr    string `json:"state_abbreviation"`
	State        string `json:"state"`
	MailableCity bool   `json:"mailable_city"`
}

type SmartyZipCode struct {
	Zip         string  `json:"zipcode"`
	ZipType     string  `json:"zipcode_type"`
	DefaultCity string  `json:"default_city"`
	CountyFips  string  `json:"county_fips"`
	CountName   string  `json:"count_name"`
	StateAbbr   string  `json:"state_abbreviation"`
	State       string  `json:"state"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
	Precision   string  `json:"precision"`
}

type SmartyZipResponse struct {
	InputIndex int              `json:"input_index"`
	CityStates []SmartyZipState `json:"city_states"`
	ZipCodes   []SmartyZipCode  `json:"zipcodes"`
}

func (self SmartyZipResponse) Response(index int) ProviderResponser { return self }

type SmartyZipResponses []SmartyZipResponse

func (self SmartyZipResponses) Response(index int) ProviderResponser {
	responses := SmartyZipResponses{}
	if index < 0 || index > len(self) {
		return responses
	}

	for _, v := range self {
		if v.InputIndex == index {
			responses = append(responses, v)
		}
	}
	return responses
}
