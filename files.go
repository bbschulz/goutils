package goutils

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type CopyOptions struct {
	NewOnly        bool
	Force          bool
	Overwrite      bool
	Recurse        bool
	Print          bool
	IgnoreFileName string
	ignoreList     []string
	ignoreLoaded   bool
}

func CopyFile(source os.FileInfo, sourceDir string, destDir string, options CopyOptions) (err error) {
	fi := source.Name()
	fiSource := filepath.Join(sourceDir, fi)
	fiDest := filepath.Join(destDir, fi)
	sourcefile, err := os.Open(fiSource)
	if err != nil {
		return err
	}

	defer sourcefile.Close()

	// see if the dest file already exists
	_, err = os.Stat(fiDest)
	exists := !os.IsNotExist(err)

	if exists && !options.Overwrite {
		// do nothing
		return nil
	}

	destfile, err := os.Create(fiDest)
	if err != nil {
		return err
	}

	defer destfile.Close()

	_, err = io.Copy(destfile, sourcefile)
	if err == nil {
		err = os.Chmod(fiDest, source.Mode())
	}

	return nil
}

func CopyDir(source string, destDir string, options CopyOptions) (err error) {

	// get ignore list
	if options.IgnoreFileName != "" && !options.ignoreLoaded {
		options.ignoreList, _ = ReadStringsFromFile(options.IgnoreFileName)
		options.ignoreLoaded = true
		blanks := make([]int, 0)

		// remove any blank lines
		for i, v := range options.ignoreList {
			if v == "" {
				blanks = append(blanks, i)
			}
		}

		// remove in reverse order
		for i := len(blanks) - 1; i >= 0; i-- {
			options.ignoreList = RemoveSliceElementString(options.ignoreList, i)
		}

	}

	// get properties of source dir
	sourceinfo, err := os.Stat(source)
	if err != nil {
		return err
	}

	err = os.MkdirAll(destDir, sourceinfo.Mode())
	if err != nil {
		return err
	}

	// get list of files already copied (if exists)
	var sourceDB = filepath.Join(destDir, ".copiedFiles")
	fmt.Println("Record keeping file: ", sourceDB)
	lastCopy, err := ReadStringsFromFile(sourceDB)
	if !options.NewOnly || options.Force {
		lastCopy = make([]string, 0)
	}

	directory, _ := os.Open(source)
	objects, err := directory.Readdir(-1)

	for _, obj := range objects {

		sourcefilepointer := filepath.Join(source, obj.Name())
		destinationfilepointer := filepath.Join(destDir, obj.Name())

		if ignoreFile(options, obj.Name()) {
			continue
		}

		if obj.IsDir() {
			// create sub-directories - recursively
			if options.Recurse {
				err = CopyDir(sourcefilepointer, destinationfilepointer, options)
				if err != nil {
					fmt.Println(err)
				}
			}
		} else {

			if !options.NewOnly || (!exists(lastCopy, sourcefilepointer) || options.Force) {
				lastCopy = append(lastCopy, sourcefilepointer)

				if options.Print {
					fmt.Println("Copying file: ", sourcefilepointer)
				}

				// perform copy
				err = CopyFile(obj, source, destDir, options)
				if err != nil {
					fmt.Println(err)
				}

			}
		}

	}

	// save the file list for next time
	if options.NewOnly {
		if err := SaveStringsToFile(sourceDB, lastCopy); err != nil {
			fmt.Println("Error storing list of files: ", err)
		}
	}

	return nil
}

// GetDir returns back an array of []FileInfo
// This can then be fed into other processes
func GetDir(source string) ([]os.FileInfo, error) {
	directory, _ := os.Open(source)

	objects, err := directory.Readdir(-1)
	if err != nil {
		return nil, err
	}

	return objects, nil
}

// SaveStringsToFile creates a file and saves the strings to the file
// Each string automatically has a linefeed added to the end
func SaveStringsToFile(file string, someStrings []string) error {
	// overwrites if already exists
	datafile, err := os.Create(file)
	defer datafile.Close()
	if err != nil {
		fmt.Println("Error creating file: ", err)
		return err
	}

	for _, v := range someStrings {
		fmt.Fprint(datafile, v, "\n")
	}
	return nil
}

// ReadStringsFromFile  is the reverse of SaveStringsToFile()
func ReadStringsFromFile(file string) ([]string, error) {
	var someStrings []string = make([]string, 0)

	datafile, err := os.Open(file)
	defer datafile.Close()
	if err != nil {
		return someStrings, err
	}

	scanner := bufio.NewScanner(datafile)
	for scanner.Scan() {
		someStrings = append(someStrings, scanner.Text())
	}

	return someStrings, nil
}

// returns true if the value exists in the array
func exists(someStrings []string, compare string) bool {
	for _, v := range someStrings {
		if strings.ToLower(v) == strings.ToLower(compare) {
			return true
		}
	}
	return false
}

// pass in the file name without path
func ignoreFile(options CopyOptions, fName string) bool {
	for _, r := range options.ignoreList {
		if match, err := filepath.Match(strings.ToLower(r), strings.ToLower(fName)); err == nil && match {
			return true
		}
	}
	return false
}
