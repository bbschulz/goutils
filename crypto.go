package goutils

import (
	"crypto/sha1"
	"encoding/base64"
)

// Sha1Hash creates a 1-way hash of the key which is then base64 encoded
// Useful for generating unique identifiers in mongo based on some combined data from the document.
func Sha1Hash(key string) string {
	hasher := sha1.New()
	hasher.Write([]byte(key))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

//
//func ComputeHmac256(message string, secret string) string {
//	key := []byte(secret)
//	h := hmac.New(sha256.New, key)
//	h.Write([]byte(message))
//	return base64.StdEncoding.EncodeToString(h.Sum(nil))
//}
//
//func (ms *MapServer) stockeVue(bv []byte) {
//	hasher := sha1.New()
//	hasher.Write(bv)
//	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
//	...
//}
//
