package goutils

func PtrStr(s string) *string {
	return &s
}

func PtrBool(b bool) *bool {
	return &b
}

func PtrInt(i int) *int {
	return &i
}
