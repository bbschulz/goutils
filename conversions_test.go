package goutils

import (
	"reflect"
	"strings"
	"testing"
)

//func StreamToByte(stream io.Reader) []byte {
//func StreamToString(stream io.Reader) string {

func TestStreamToByte(t *testing.T) {
	var cases = []struct {
		input    string
		expected []byte
	}{
		{
			input:    "Test String",
			expected: []byte("Test String"),
		},
	}

	for _, test := range cases {
		reader := strings.NewReader(test.input)
		compare := StreamToByte(reader)
		if !reflect.DeepEqual(compare, test.expected) {
			t.Errorf("Input: %q, Expected: %q, Got: %q", test.input, test.expected, compare)
		}
	}
}

func TestStreamToString(t *testing.T) {
	var cases = []struct {
		input    string
		expected string
	}{
		{
			input:    "Test String",
			expected: "Test String",
		},
	}

	for _, test := range cases {
		reader := strings.NewReader(test.input)
		compare := StreamToString(reader)
		if !reflect.DeepEqual(compare, test.expected) {
			t.Errorf("Input: %q, Expected: %q, Got: %q", test.input, test.expected, compare)
		}
	}
}
