package goutils

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

// NOTE:  the use of this file is deprecated.  It is replaced with address/smarty.go.
// it is being kept for a while until clients using this are refactored.

const (
	match string = "strict" // strict || range || invalid
	host  string = "us-street.api.smartystreets.com"
	path  string = "street-address"
)

type SmartyRequest struct {
	InputId    string `json:"input_id,omitempty"`
	StreetNo   string `json:"-"`
	Street     string `json:"street"`
	City       string `json:"city"`
	State      string `json:"state"`
	Zip        string `json:"zipcode"`
	Match      string `json:"match,omitempty"`
	Candidates int    `json:"candidates,omitempty"`
}

type SmartyRequests struct {
	Requests  []SmartyRequest
	Responses []SmartyResponse
}

type SmartyMetadata struct {
	RecordType   string  `json:"record_type"`
	ZipType      string  `json:"zip_type"`
	CountyFips   string  `json:"county_fips"`
	CountyName   string  `json:"county_name"`
	CarrierRoute string  `json:"carrier_route"`
	RDI          string  `json:"rdi"`
	ElotSequence string  `json:"elot_sequence"`
	ElotSort     string  `json:"elot_sort"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Precision    string  `json:"precision"`
	TimeZone     string  `json:"time_zone"`
	UtcOffset    int64   `json:"utc_offset"`
	DST          bool    `json:"dst"`
}

type SmartyAnalysis struct {
	DpvMatchCode string `json:"dpv_match_code"`
	DpvFootnotes string `json:"dpv_footnotes"`
	DpvCmra      string `json:"dpv_cmra"`
	DpvVacant    string `json:"dpv_vacant"`
	Active       string `json:"active"`
	Footnotes    string `json:"footnotes"`
}

type SmartyResponse struct {
	InputIndex     int            `json:"input_index"`
	CandidateIndex int            `json:"candidate_index"`
	DeliveryLine1  string         `json:"delivery_line_1"`
	LastLine       string         `json:"last_line"`
	Metadata       SmartyMetadata `json:"metadata"`
	Analysis       SmartyAnalysis `json:"analysis"`
}

type smarty struct {
	Host      string
	Path      string
	AuthId    string
	AuthToken string
	Client    http.Client
}

func NewSmarty(auth string, token string) {
	s := smarty{
		Host:      host,
		Path:      path,
		AuthId:    auth,
		AuthToken: token,
		Client:    http.Client{},
	}
	Smarty = s
	return
}

var Smarty smarty = smarty{}

func (self *smarty) Lookup(street string, city string, state string, zip string) []SmartyResponse {
	values := url.Values{
		"auth-id":    {self.AuthId},
		"auth-token": {self.AuthToken},
		"street":     {street},
		"city":       {city},
		"state":      {state},
		"zipcode":    {zip},
		"match":      {match},
		"candidates": {"2"},
	}
	response := make([]SmartyResponse, 0)

	err := self.get(values, &response)
	if err != nil {
		fmt.Println(err)
	}
	return response
}

func (self *smarty) get(values url.Values, result interface{}) error {
	request, _ := http.NewRequest("GET", "https://"+host+"/"+path+"?"+values.Encode(), nil)
	//request.Header.Set("Host", host)
	//request.Header.Set("Content-Type", "application/json")
	if resp, err := self.Client.Do(request); err != nil {
		return err
	} else if err = json.NewDecoder(resp.Body).Decode(result); err != nil {
		return err
	}
	return nil
}

func (self *smarty) post(s []SmartyRequest, result interface{}) error {
	jsonBody, err := json.Marshal(s)
	if err != nil {
		return err
	}

	values := url.Values{
		"auth-id":    {self.AuthId},
		"auth-token": {self.AuthToken},
	}

	request, _ := http.NewRequest("POST", "https://"+host+"/"+path+"?"+values.Encode(), bytes.NewBuffer(jsonBody))
	request.Header.Set("Host", host)
	request.Header.Set("Content-Type", "application/json")
	//fmt.Println(request.URL)
	resp, err := self.Client.Do(request)
	if err != nil {
		return err
	}
	if resp.Status != "200 OK" {
		log.Println("Response status: ", resp.Status)
	}
	if err = json.NewDecoder(resp.Body).Decode(result); err != nil {
		return err
	}
	return nil
}

func (self *SmartyRequests) Lookup() error {

	// make sure not over 100
	if len(self.Requests) > 100 {
		return errors.New("More than 100 request records")
	}

	response := make([]SmartyResponse, 0)
	requests := make([]SmartyRequest, 0)

	// assemble the street with the streetNO
	for _, v := range self.Requests {
		v.Street = v.StreetNo + " " + v.Street
		requests = append(requests, v)
	}

	err := Smarty.post(requests, &response)
	if err != nil {
		return err
	}
	self.Responses = response

	return nil
}

func (self *SmartyRequests) Response(index int) []SmartyResponse {
	response := make([]SmartyResponse, 0)
	if index < 0 || index > len(self.Requests) {
		return response
	}

	for _, v := range self.Responses {
		if v.InputIndex == index {
			response = append(response, v)
		}
	}
	return response
}
