package goutils

import "testing"

func TestIncrString(t *testing.T) {
	var testCases = []struct {
		str1     string
		str2     string
		incr     int
		expected []string
	}{
		{"-0600", "-0602", 1, []string{"00600", "00601", "00602"}},
		{"-600", "-602", 1, []string{"0600", "0601", "0602"}},
		{"-001", "-002", 1, []string{"0001", "0002"}},
		{"10A", "14A", 2, []string{"10A", "12A", "14A"}},
		{"10A", "10C", 1, []string{"10A", "10B", "10C"}},
		{"10A", "10C", 2, []string{"10A", "10C"}},
		{"100-200", "100-202", 1, []string{"100-200", "100-201", "100-202"}},
		{"100-200", "100-202", 2, []string{"100-200", "100-202"}},
		{"100/10", "100/12", 1, []string{"100/10", "100/11", "100/12"}},
		{"100/10", "100/12", 2, []string{"100/10", "100/12"}},
		{"00100", "00101", 1, []string{"100", "101"}},
	}

	for _, test := range testCases {
		r, err := GenRange(test.str1, test.str2, test.incr)
		if err != nil {
			t.Errorf("Error str1=%q, str2=%q, incr=%q error=%v", test.str1, test.str2, test.incr, err)
		}
		if !compareSlices(r, test.expected) {
			t.Errorf("Test: str1=%q str2=%q incr=%v expected=%v got=%v", test.str1, test.str2, test.incr, test.expected, r)
		}
	}
}
