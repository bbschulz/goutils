package goutils

import "testing"

func TestStringInSlice(t *testing.T) {
	testSlice := []string{
		"Test 1324",
		"Test 444",
		"abc",
	}

	if StringInSlice("ABC", testSlice) {
		t.Error()
	}

	if !StringInSlice("ABC", testSlice, true) {
		t.Error()
	}

	if !StringInSlice("abc", testSlice) {
		t.Error()
	}
}
