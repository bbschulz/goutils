package goutils

import (
	"fmt"
	"math"
	"strconv"
	"sync"
	"time"
)

const TIME_FORMAT = "2006-01-02 15:04:05"

var Stats stats = make(map[string]int32)

var StatsMutex sync.Mutex

type stats map[string]int32

func (self stats) Set(key string, val int32) {
	StatsMutex.Lock()
	self[key] = val
	StatsMutex.Unlock()
}
func (self stats) Incr(key string) {
	StatsMutex.Lock()
	if v, ok := self[key]; ok {
		self[key] = v + 1
	} else {
		self[key] = 1
	}
	StatsMutex.Unlock()
}
func (self stats) IncrBy(key string, n int32) {
	StatsMutex.Lock()
	if v, ok := self[key]; ok {
		self[key] = v + n
	} else {
		self[key] = n
	}
	StatsMutex.Unlock()
}
func (self stats) Get(key string) int32 {
	StatsMutex.Lock()
	r := self[key]
	StatsMutex.Unlock()
	return r
}

type perfCounter struct {
	name     string
	start    time.Time
	stop     time.Time
	duration time.Duration
}

func (self *perfCounter) Stop() {
	self.stop = time.Now()
	self.duration = time.Since(self.start)
	Perf.count(self)
}

// todo: performance stats do not track actual elapsed time.  The total durration does not work right.

type perfAgg struct {
	Name       string
	Count      int64
	Min        time.Duration
	Max        time.Duration
	Avg        time.Duration
	Tot        time.Duration
	Throughput int64
	start      time.Time
}

//func (self perfAgg) String() string {
//	return fmt.Sprintf("%v: Name:  %15s Count: %10d Min: %s Max: %s Avg: %s TPut: %d \n",
//		time.Now().Format(TIME_FORMAT),
//		self.Name,
//		self.Count,
//		self.Min,
//		self.Max,
//		self.Avg,
//		self.Throughput)
//}

var Perf *perfAggs = &perfAggs{counters: make(map[string]*perfAgg), order: make([]string, 0), start: time.Now()}

type perfAggs struct {
	counters map[string]*perfAgg
	order    []string
	maxLen   int
	start    time.Time
	sync.Mutex
}

func (self *perfAggs) Start(name string) *perfCounter {
	return &perfCounter{
		name:  name,
		start: time.Now(),
	}
}

func (self *perfAggs) count(count *perfCounter) {
	self.Lock()
	defer self.Unlock()

	a := self.counters[count.name]
	if a == nil {
		// keeps track of the order the counters were created
		self.order = append(self.order, count.name)

		a = &perfAgg{
			Name:       count.name,
			Count:      0,
			Min:        count.duration,
			Max:        count.duration,
			Avg:        0,
			start:      count.start,
			Throughput: 0,
		}

		self.counters[count.name] = a
	}

	a.Count++
	a.Tot += count.duration
	a.Min = minDur(a.Min, count.duration)
	a.Max = maxDur(a.Max, count.duration)
	a.Avg = a.Tot / time.Duration(a.Count)
	if time.Since(a.start).Seconds() > 1 {
		a.Throughput = a.Count / int64(time.Since(a.start).Seconds())

	}
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func (self perfAggs) String() string {
	self.Lock()
	defer self.Unlock()

	for _, c := range self.counters {
		self.maxLen = max(self.maxLen, len(c.Name))
	}

	output := ""
	for _, a := range self.order {
		output = output + fmt.Sprintf("%v: Elapsed: %6s  Name:  %"+strconv.Itoa(self.maxLen)+"s  Count: %10d  Min: %6s  Max: %6s  Avg: %6s  TPut: %d \n",
			time.Now().Format(TIME_FORMAT),
			time.Since(self.start),
			self.counters[a].Name,
			self.counters[a].Count,
			self.counters[a].Min,
			self.counters[a].Max,
			self.counters[a].Avg,
			self.counters[a].Throughput)
	}
	return output
}

func (self *perfAggs) Reset(name string) {
	self.Lock()
	defer self.Unlock()

	self.counters[name] = &perfAgg{
		Name:  name,
		Count: 0,
		Min:   0,
		Max:   0,
		Avg:   0,
	}
}

func (self *perfAggs) ResetAll() {
	self.Lock()
	defer self.Unlock()

	self.counters = make(map[string]*perfAgg)
	self.order = make([]string, 0)
}

func (self *perfAggs) Get(name string) *perfAgg {
	self.Lock()
	defer self.Unlock()

	a, ok := self.counters[name]
	if !ok {
		return &perfAgg{Name: name}
	}

	return a
}

func (self *perfAggs) GetAll() map[string]*perfAgg {
	self.Lock()
	defer self.Unlock()

	return self.counters
}

// helper function to convert types
func minDur(x time.Duration, y time.Duration) time.Duration {
	xf := float64(x)
	yf := float64(y)
	min := math.Min(xf, yf)
	return time.Duration(min)
}

// helper function to convert types
func maxDur(x time.Duration, y time.Duration) time.Duration {
	xf := float64(x)
	yf := float64(y)
	min := math.Max(xf, yf)
	return time.Duration(min)
}
