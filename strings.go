// strings is a collection of utility functions primarily dealing with strings.

package goutils

import (
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// strIncrement increments any string of numbers, similar to Excel.  Even containing alpha numeric characters
// 1000 -> 1001
// 100a -> 100B
// 100-100 -> 100-101
// 100-- -> error!
// n is an optional argument that specifies how many to increment.

const (
	RE_NUMBERS_ONLY = iota
	RE_LETTERS_ONLY
	RE_SIGNIFICANT_ZERO
	RE_DASH_NUMBER
	RE_FRACTION
	RE_LETTER_SUFFIX
	RE_DASH
	RE_NUMBERS
	RE_LETTERS
	RE_LEADING_ZEROS
)

var patterns = map[int]*regexp.Regexp{
	RE_NUMBERS_ONLY:     regexp.MustCompile("^[0-9]+$"),
	RE_LETTERS_ONLY:     regexp.MustCompile("^[A-Za-z]+$"),
	RE_SIGNIFICANT_ZERO: regexp.MustCompile("^[0]*(-0*)[0-9]+$"),         // 000-0143 -> streeno: 00143
	RE_DASH_NUMBER:      regexp.MustCompile("^0+[1-9]+[0-9]*[-][0-9]+$"), // 000000143-12 -> 143-12
	RE_FRACTION:         regexp.MustCompile("^[0-9]+[ ]+[0-9]+/[0-9]+$"), // 3343 1/2
	RE_LETTER_SUFFIX:    regexp.MustCompile("^[0-9]+[A-Za-z]$"),          // 000003343A -> 3343A
	RE_DASH:             regexp.MustCompile("-"),
	RE_NUMBERS:          regexp.MustCompile("[0-9]+"),
	RE_LETTERS:          regexp.MustCompile("[A-Za-z]+"),
	RE_LEADING_ZEROS:    regexp.MustCompile("^(0+)[0-9]+$"),
}

// incrstring is deprecated
func IncrString(str string, n ...int) (string, error) {
	var incr int = 1
	if len(n) > 0 {
		incr = n[0]
	}

	switch {
	case patterns[RE_NUMBERS_ONLY].MatchString(str):
		i, err := strconv.Atoi(str)
		if err == nil {
			return strconv.Itoa(i + incr), nil
		}

	case patterns[RE_SIGNIFICANT_ZERO].MatchString(str):
		// ignoring these
		return "", errors.New("Ignoring significant zero")
	case patterns[RE_DASH_NUMBER].MatchString(str):
		// must be a special char in the number string
		specChar := patterns[RE_DASH_NUMBER].FindAllStringIndex(str, -1)
		// index to the rightmost match.  Each match is a slice of [start:end] for the match
		nRight := specChar[len(specChar)-1]
		// save off the right-most special character(s)
		specChars := str[nRight[0]:nRight[1]]
		// if there is no numeric string on the right (e.g. 101--), return error
		if _, err := strconv.Atoi(str[nRight[1]:]); err != nil {
			return "", errors.New("Rightmost part of string is non-numeric")
		}
		// get the rightmost numeric part of the string & increment
		strEval := str[nRight[1]:]
		newVal, err := strconv.Atoi(strEval)
		if err == nil {
			strEval = strconv.Itoa(newVal + incr)
			// reassemble the final string
			str = str[:nRight[0]] + specChars + strEval
			return str, nil
		} else {
			return "", err
		}

	case patterns[RE_FRACTION].MatchString(str):
		// ignore these
		return "", errors.New("Ignoring faction in address")
	case patterns[RE_LETTER_SUFFIX].MatchString(str):
		// is the last digit a letter?
		// convert to base 36, add 1, then convert back to string
		// note this operates in uppercase only
		if matched, err := regexp.MatchString("[A-Za-z]$", str); err == nil && matched {
			lastDigit, err := strconv.ParseInt(str[len(str)-1:], 36, 64)
			if err == nil {
				// if a Z, then roll around to A
				lastDigit += int64(incr)
				if lastDigit > 35 {
					lastDigit = 10
				}
				return strings.ToUpper(str[:len(str)-1] + strconv.FormatInt(lastDigit, 36)), nil
			} else {
				log.Fatalln(err)
				return "", errors.New("Could not string character value to Integer " + str)
			}
		}
	}

	return str, nil

}

func incrString(str string, n int) (string, error) {
	var incr int = n

	if patterns[RE_NUMBERS_ONLY].MatchString(str) {
		i, err := strconv.Atoi(str)
		if err == nil {
			return strconv.Itoa(i + incr), nil
		}
	} else {
		// only operate on the last digit
		lastDigit, err := strconv.ParseInt(str[len(str)-1:], 36, 64)
		if err == nil {
			// if a Z, then roll around to A
			lastDigit += int64(incr)
			if lastDigit > 35 {
				lastDigit = 10
			}
			return strings.ToUpper(str[:len(str)-1] + strconv.FormatInt(lastDigit, 36)), nil
		} else {
			log.Fatalln(err)
			return "", errors.New("Could not string character value to Integer " + str)
		}

	}

	return str, nil

}

// given a starting and ending string, generate the range
// strings must be the same length and have the same general structure
// only a section containing consecutive numbers will be incremented
// or if a letter suffix or prefix only that will be incremented
func GenRange(str1, str2 string, n ...int) ([]string, error) {
	incr := 1
	if len(n) > 0 {
		incr = n[0]
	}

	var z string
	if patterns[RE_SIGNIFICANT_ZERO].MatchString(str1) {
		// strip off the leading significant zero's
		// will add them back later
		z = patterns[RE_SIGNIFICANT_ZERO].FindStringSubmatch(str1)[1]
		str1 = strings.Replace(str1, z, "", 1)
		str2 = strings.Replace(str2, z, "", 1)
		z = strings.Replace(z, "-", "0", -1)

	}
	if patterns[RE_LEADING_ZEROS].MatchString(str1) {
		// strip off leading zero's
		leading := patterns[RE_LEADING_ZEROS].FindStringSubmatch(str1)[1]
		str1 = strings.Replace(str1, leading, "", 1)
		str2 = strings.Replace(str2, leading, "", 1)
	}
	newRange := []string{z + str1}

	switch {
	case str1 == str2:
		//nothing to do if the strings match
		return newRange, nil

	default:
		//newRange = append(newRange, str2)
		diff := difference(str1, str2)
		if diff.starting != "" {
			cmp := diff.starting
			breaker := 0
			for cmp < diff.ending {
				next, err := incrString(cmp, incr)
				if err != nil || breaker > 1000 || next == cmp {
					return newRange, errors.New(fmt.Sprint("Breakder: :", breaker, "or last: ", cmp, "=next: ", next, "or Error: ", err))
				}
				newRange = append(newRange, z+diff.assemble(next))
				breaker++
				cmp = next
			}
		}
	}

	return newRange, nil
}

func MakeStrPtr(s string) *string {
	return &s
}

type strDiff struct {
	// original string
	oString string

	// string to compare with
	cString string

	// the part of the string that is different in oString
	starting string

	// the part of the string that is different in cString
	ending string

	// the starting index of the different characters
	startIndex int

	// the ending index of the different characters
	endIndex int

	// 0 = no error
	// 1 = difference is not sequential
	err int
}

// for the input str, this assembles a new string replacing the
// characters that are different
func (diff strDiff) assemble(str string) string {
	var endPoint int = diff.endIndex
	if endPoint > len(diff.oString) {
		endPoint = len(diff.oString)
	}
	var startPoint int = diff.startIndex
	if startPoint < 0 {
		startPoint = 0
	}
	return diff.oString[0:diff.startIndex] + str + diff.oString[endPoint:]
}

func min(n1, n2 int) int {
	if n1 < n2 {
		return n1
	}
	return n2
}

// look for different letter chunk
func cmp(str1, str2 string, ind1, ind2 [][]int) (strDiff, bool) {
	//fmt.Println("comparing str1: ", str1, "str2: ", str2, "ind1: ", ind1, "ind2: ", ind2)
	diff := strDiff{oString: str1, cString: str2}
	found := false
	for i := 0; i < min(len(ind1), len(ind2)); i++ {
		if str1[ind1[i][0]:ind1[i][1]] != str2[ind2[i][0]:ind2[i][1]] {
			diff.starting = str1[ind1[i][0]:ind1[i][1]]
			diff.ending = str2[ind2[i][0]:ind2[i][1]]
			diff.startIndex = ind1[i][0]
			diff.endIndex = ind1[i][1]
			found = true
			break
		}
	}
	return diff, found
}

// compare 2 strings to find the difference
// looks for groups of letters or numbers
// spaces or dashes are ignored
// finds the first different chunk of letters or numbers
func difference(str1 string, str2 string) strDiff {

	var diff strDiff
	var ok bool

	// check for letter chung differences
	diff, ok = cmp(str1, str2, patterns[RE_LETTERS].FindAllStringIndex(str1, -1), patterns[RE_LETTERS].FindAllStringIndex(str2, -1))
	if ok {
		//fmt.Println("found letters")
		return diff
	}

	// check for number chunk differences
	diff, ok = cmp(str1, str2, patterns[RE_NUMBERS].FindAllStringIndex(str1, -1), patterns[RE_NUMBERS].FindAllStringIndex(str2, -1))
	if ok {
		//fmt.Println("found numbers")
		return diff
	}

	return strDiff{}
}
