package config

import (
	"reflect"
	"testing"
)

type testMap map[string]string

type testStruct struct {
	a string
	b int
}

func TestGlobals(t *testing.T) {
	var i int = -4
	var i64 int64 = 32768
	var f64 float64 = -3.14159267
	var s string = "hello"
	m := testMap{"test": "Map"}
	stct := testStruct{a: "hello", b: 32}

	Set("int", i)
	ti := GetInt16("int")
	if ti != i {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "int", i, ti)
	}

	Set("int64", i64)
	ti64 := GetInt64("int64")
	if ti64 != i64 {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "int64", i64, ti64)
	}

	Set("float64", f64)
	tf64 := GetFloat64("float64")
	if tf64 != f64 {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "float64", f64, tf64)
	}

	Set("string", s)
	ts := GetString("string")
	if ts != s {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "string", s, ts)

	}

	Set("StringLower", s)
	ts = GetString("stringlower")
	if ts != s {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "stringlower", s, ts)

	}

	Set("StringCaps", s)
	ts = GetString("STRINGCAPS")
	if ts != s {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "STRINGCAPS", s, ts)

	}

	Set("map", m)
	tm := Get("map").(testMap)
	if !reflect.DeepEqual(m, tm) {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "map", m, tm)

	}

	Set("struct", stct)
	tstct := Get("struct").(testStruct)
	if !reflect.DeepEqual(stct, tstct) {
		t.Errorf("Test: %s, Expected: %v, Got: %v", "struct", stct, tstct)
	}
}
