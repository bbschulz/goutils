package config

import (
	"reflect"
	"sync"

	"strings"

	"github.com/lookingcloudy/logrus"
)

var (
	mutex sync.RWMutex
	data  = make(map[string]interface{})
)

func Set(key string, val interface{}) {
	mutex.Lock()
	defer mutex.Unlock()
	key = strings.ToLower(key)

	if key != "" {
		logrus.Debugf("Setting global config %q %q \n", key, val)
		data[key] = val
	}
}

func Get(key string) interface{} {
	val := get(key)
	return val
}

// GetString converts interface{} to a string
func GetString(key string) string {
	v := get(key)
	if reflect.TypeOf(v).Kind() == reflect.String {
		logrus.Debugf("String config value found: %q %q \n", key, reflect.ValueOf(v).String())
		return reflect.ValueOf(v).String()
	}
	logrus.Debugf("String config value not found: %q \n", key)
	return ""
}

func GetInt64(key string) int64 {
	v := get(key)
	if reflect.TypeOf(v).Kind() == reflect.Int64 {
		logrus.Debugf("Int64 config value found:%q %q \n ", key, reflect.ValueOf(v).Int())
		return reflect.ValueOf(v).Int()
	}
	logrus.Debugf("Int64 config value not found: %q \n", key)
	return 0
}

func GetInt16(key string) int {
	v := get(key)
	if reflect.TypeOf(v).Kind() == reflect.Int {
		logrus.Debugf("Int64 config value found: %q, %q \n", key, reflect.ValueOf(v).Int())
		return int(reflect.ValueOf(v).Int())
	}
	logrus.Debug("Int64 config value not found: ", key)
	return 0
}

func GetFloat64(key string) float64 {
	v := get(key)
	if reflect.TypeOf(v).Kind() == reflect.Float64 {
		logrus.Debugf("float64 config value found: %q %q \n", key, reflect.ValueOf(v).Float())
		return reflect.ValueOf(v).Float()
	}
	logrus.Debugf("Float64 config value not found: %q \n ", key)
	return 0.0
}

// Data in the global configuration object is stored as a hierarchy.  The value can be a map of maps of values.
// To access individual values or inner objects use dot notation such as "key.key.key".
func get(key string) interface{} {
	mutex.RLock()
	defer mutex.RUnlock()

	key = strings.ToLower(key)

	var val interface{}
	keys := strings.Split(key, ".")
	val = data
	for _, k := range keys {
		val = mapValue(val, k)
		if val == nil {
			// key does not exist
			//debug.PrintStack()
			logrus.Warnf("Config value does not exist %q, %q \n", key, val)
		}
	}

	logrus.Debugf("Config value found: %q, %q \n", key, val)

	if val == nil {
		val = ""
	}
	return val
}

// returns input if not a map, or returns the value if it is a map and the key exists
func mapValue(i interface{}, key string) interface{} {
	iVal := reflect.ValueOf(i)
	if iVal.Kind() == reflect.Map {
		iMap := iVal.Interface().(map[string]interface{})
		val := iMap[key]
		// nil if not found
		return val
	}
	return i
}
