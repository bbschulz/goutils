/*
	Contains system configuration structures
*/

package config

import (
	"log"
	"os"

	"path/filepath"

	"reflect"

	"strings"

	"github.com/BurntSushi/toml"
)

func GetConfig(searchPaths []string, configFile string) {
	ok := false
	initConfig := make(map[string]interface{})
	for _, p := range searchPaths {
		ok = exists(p, configFile)
		if ok {
			if _, err := toml.DecodeFile(filepath.Join(p, configFile), &initConfig); err != nil {
				log.Fatal("Cannot read config file: ", err)
			}
			break
		}
	}
	if !ok {
		log.Fatal("Config file does not exist: ", configFile)
	}

	lowerConfig := lowerKeys(initConfig)

	// iterates through the map and creates vars
	Set("config", lowerConfig)
	//setRange(initConfig, "config")
	return
}

// provides a direct path to the config file
func GetConfigFile(configFile string) {
	initConfig := make(map[string]interface{})
	if _, err := toml.DecodeFile(configFile, &initConfig); err != nil {
		log.Fatal("Cannot read config file: ", err)
	}

	// convert all keys to lowercase
	lowerConfig := lowerKeys(initConfig)

	// iterates through the map and creates vars
	Set("config", lowerConfig)
	return
}

func lowerKeys(m map[string]interface{}) map[string]interface{} {
	newMap := make(map[string]interface{})
	for k, v := range m {
		if reflect.TypeOf(v).Kind() == reflect.Map {
			newMap[strings.ToLower(k)] = lowerKeys(v.(map[string]interface{}))
		} else {
			newMap[strings.ToLower(k)] = v
		}
	}
	return newMap
}

func setRange(m map[string]interface{}, parentKey string) {
	for k, v := range m {
		if parentKey != "" {
			k = parentKey + "." + k
		}
		if reflect.TypeOf(v).Kind() == reflect.Map {
			i := reflect.ValueOf(v).Interface()
			s := i.(map[string]interface{})
			setRange(s, k)
		} else {
			// all config keys use lower case
			Set("config."+strings.ToLower(k), v)
		}
	}
	return
}

func exists(path string, file string) bool {
	folderPath, _ := os.Getwd()
	checkFile := filepath.Join(folderPath, path, file)
	if _, err := os.Stat(checkFile); os.IsNotExist(err) {
		return false
	}
	return true
}
